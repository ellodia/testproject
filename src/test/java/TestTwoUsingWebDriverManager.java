import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestTwoUsingWebDriverManager {
    static WebDriver firefoxDriver;
    static WebDriver chromeDriver;

    @Before
    public void setup() {
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();

    }

    @Test
    public void test01YandexPage() {
        chromeDriver = new ChromeDriver();
        chromeDriver.get("https://yandex.ru");
        chromeDriver.manage().window().maximize();
    }

    @Test
    public void test02YandexPage() {
        firefoxDriver = new FirefoxDriver();
        firefoxDriver.get("https://yandex.ru");
        firefoxDriver.manage().window().maximize();
    }

//    @After
//    public void ClosePage() {
//        chromeDriver.close();
//        firefoxDriver.close();
//
//    }

    @AfterClass
    public static void ClosePage() {
        chromeDriver.close();
        firefoxDriver.close();
    }
}
