import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class LocatorsTest {
//    static WebDriver firefoxDriver;
    static WebDriver chromeDriver;

    @Before
    public void setup() {
        WebDriverManager.chromedriver().setup();
//        WebDriverManager.firefoxdriver().setup();

    }

    @Test
    public void Test01GooglePage() {
        int expectedCount = 120;

        chromeDriver = new ChromeDriver();
        chromeDriver.manage().window().maximize();
        chromeDriver.get("https://www.google.com/?hl=ru");
        WebElement search = chromeDriver.findElement(By.name("q"));
        search.sendKeys("CSS vs XPATH locators");
        search.submit();
        List<WebElement> links = chromeDriver.findElements(By.xpath("//a"));
        if (expectedCount == links.size()){
            System.out.printf("Links count equal expected = %d \n", expectedCount);
        }
        else {
            System.out.printf("Links count not equal expected = %d, given = %d \n", expectedCount, links.size());
        }
        System.out.println(links.get(0));

    }

//    @Test
//    public void Test02FirefoxPage() {
//        firefoxDriver = new FirefoxDriver();
//        firefoxDriver.get("https://google.com");
//        firefoxDriver.manage().window().maximize();
//    }

//    @After
//    public void ClosePage() {
//        chromeDriver.close();
//        firefoxDriver.close();
//
//    }

//    @AfterClass
//    public static void ClosePage() {
////        chromeDriver.close();
////        firefoxDriver.close();
//    }
}
